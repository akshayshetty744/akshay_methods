let data = require("./data.js");

// Q1  Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.
function countAllPeople(data) {
  return (houseLength = data.got.houses
        .map((e) => e.people.length)
        .reduce((acc, e) => acc + e)); // adding  value from people.langth
}
console.log(countAllPeople(data));


// Q2  Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.
function peopleByHouses(data) {
    let object = data.got.houses.reduce((arr, e) => {
        arr[e.name] = e.people.length;
        return arr;
    }, {});
    // converting object to array for sorting
    let arrayOfObject = Object.entries(object).sort();
    //from array as we have done in previous step we are again converting into object
    console.log(Object.fromEntries(arrayOfObject));
    // console.log(Object.fromEntries(arrayOfObject.reverse()));//reverse object
}
peopleByHouses(data);

// Q3 Write a function called `everyone` which returns a array of names of all the people in `got` variable.
function everyone() {
    let result = [];
    data.got.houses.map((e) => {
        e.people.map((ele) => {
        result.push(ele.name);
        })
    })
  console.log(result);
}
everyone(data);


// Q4 Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.
function nameWithS(data) {
  let result = [];
      data.got.houses.map((e) => {
          e.people.map((ele,i) => {
              let val = ele.name;
                return  result.push(ele.name);
          })
      })
    return  result.filter(element => {
          return element.split('').find(items => {
              if (items == 's' || items == 'S') {
                  return true;
            }
          });
      });
}
console.log(nameWithS(data)) ;

// Q5 Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.
function nameWithA(data) {
    let result = [];
        data.got.houses.map((e) => {
            e.people.map((ele,i) => {
                let val = ele.name;
                  return  result.push(ele.name);
            })
        })
      return  result.filter(element => {
            return element.split('').find(items => {
                if (items == 'a' || items == 'A') {
                    return true;
                }
            });
        });
}
console.log(nameWithA(data));

// Q6 Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).
function surnameWithS(data) {
  let result = [];
          data.got.houses.map((e) => {
              e.people.map((ele,i) => {
                  let val = ele.name;
                    return  result.push(ele.name);
              })
          })
        return  result.filter(element => {
            return element.split(" ")[1][0] == "S";
          });
}
console.log(surnameWithS(data));

// Q7 Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).
function surnameWithA(data) {
  let result = [];
     data.got.houses.map((e) => {
        e.people.map((ele,i) => {
               let val = ele.name;
               return  result.push(ele.name);
        })
    })
     return  result.filter(element => {
       return element.split(" ")[1][0] == "A";
    });
}
console.log(surnameWithA(data));

// Q8 Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.
function peopleNameOfAllHouses(data) {
    let obj = {}
    data.got.houses.forEach((ele) => {
        if (!obj[ele.name]) {
            obj[ele.name] = [];
        }  
        for (let el of ele.people) {
            obj[ele.name].push(el.name)
        }
    })
  return Object.fromEntries(Object.entries(obj).sort());
}
console.log(peopleNameOfAllHouses(data));

// // Testing your result after writing your function
// console.log(countAllPeople(data));
// // Output should be 33

// // Output should be
// //{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

// console.log(everyone());
// // Output should be
// //["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

// console.log(nameWithS(), 'with s');
// // Output should be
// // ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

// console.log(nameWithA());
// // Output should be
// // ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

// console.log(surnameWithS(), 'surname with s');
// // Output should be
// // ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

// console.log(surnameWithA());
// // Output should be
// // ["Lysa Arryn", "Jon Arryn"]

// console.log(peopleNameOfAllHouses());
// // Output should be
// // {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}
